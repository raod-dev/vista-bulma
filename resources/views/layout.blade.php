<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>INTRANET ARTHA - BULMA</title>
    <link rel="shortcut icon" href="{{ asset ('favicon.ico')}}"/>
    <link rel="stylesheet" href="{{ asset('css/bulmaswatch.min.css') }}">
    <style>
    .hero .container {
      width: 95%;
    }
    </style>
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
  </head>

      @yield('contenido')

  <script>
if (document.location.hostname !== 'localhost') {
  (function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
      m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

  ga('create', 'UA-91125324-1', 'auto');
  ga('send', 'pageview');
} else {
  window.ga = function ga() {
    console.log('[ga]', arguments); // mock GA to prevent errors
  }
}
</script>
<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js')}}"></script>
<script type="text/javascript">
$(function() {
  $('.modal-background, .modal-close').click(function() {
    $('html').removeClass('is-clipped');
    $(this).parent().removeClass('is-active');
  });
  $('.theme-switcher').change(function(e) {
    if (e.target.value) {
      window.location.href = '/bulmaswatch/' + e.target.value;
    }
  });

  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any nav burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener('click', () => {

        // Get the target from the "data-target" attribute
        var target = $el.dataset.target;
        var $target = document.getElementById(target);

        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }
});
</script>

</html>
